The Codee approach to coding websites.

# CSS

Mostly a good practices are explained [here](https://github.com/airbnb/css): 

1. We using [BEMIT](https://www.jamesturneronline.net/blog/bemit-naming-convention.html) convenction

# HTML

1. Code should be checked with HTML Validator as you can

# Javascript

Mostly a good practices are explained [here](https://github.com/airbnb/javascript): 

1. JavaScript code should be always linted. In our starter we using [Standardjs](https://standardjs.com/)
2. JavaScript code should be commented

# Web Applications

1. [Vue.js](https://vuejs.org/) 
   - [Style guide](https://vuejs.org/v2/style-guide/)
   - [Examples](https://vuejsexamples.com/)

# Files

1. .png/.jpg/.svg should be always compressed on dev and prod version
2. Images from CMSa should be in ./files in root path
3. Pictures should be saved as .jpg format
4. Icons should be saved as .svg format

# Production

1. Code and assets should be always compressed before pushed to production

# GIT

Mostly a good practices are explained [here](https://github.com/trein/dev-best-practices/wiki/Git-Commit-Best-Practices): 

# Supported browsers

1. IE11
2. Microsoft Edge
3. Firefox - latest
4. Chrome - latest
5. Safari - latest

# Front-end starter

1. [Starter](https://bitbucket.org/iamcodee/front-end-starter/) which one we begining a new projects.

# Resources

1. http://javascript.info/